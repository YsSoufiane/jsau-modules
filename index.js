const categories = ['Sport', 'News', 'Celebrities'];
function validation(category) {
  return categories.includes(category));
     
}
module.exports = {
  validation,
  categories
};
